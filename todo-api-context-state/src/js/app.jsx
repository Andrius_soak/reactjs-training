// Please ensure that this is always top
import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';

import {BrowserRouter, Route, Switch} from 'react-router-dom';

import {Header} from './shared/header.jsx';
import {List} from './list/list.jsx';
import {About} from './about/about.jsx';

/**
 * App component.
 */
class App extends React.Component {
  /**
   * Render.
   * @return {JSX} - template.
   */
  render() {
    return (
      <BrowserRouter>
        <React.Fragment>
          <Header></Header>
          <Switch>
            <Route exact path="/" component={List}/>
            <Route exact path="/about" component={About}/>
          </Switch>
        </React.Fragment>
      </BrowserRouter>
    );
  }
}

ReactDOM.render(<App/>, document.getElementById('contents'));

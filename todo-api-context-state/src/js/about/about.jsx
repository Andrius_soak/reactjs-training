import React from 'react';

/**
 * About component.
 */
export class About extends React.Component {
  /**
   * Render.
   * @return {JSX} - template.
   */
  render() {
    return (
      <div className="about">
        <h2 className="about__title">About</h2>
        <p>This is TodoList v1.0.0</p>
      </div>
    );
  }
}

import React from 'react';
import PropTypes from 'prop-types';

import uuidv1 from 'uuid/v1';

const TodoContext = React.createContext();

/**
 * Todo Provider component.
 */
export class TodoProvider extends React.Component {
  state = {
    todos: [{
      id: 0,
      done: false,
      title: 'Get shopping'
    }, {
      id: 1,
      done: true,
      title: 'Have coffee'
    }, {
      id: 2,
      done: false,
      title: 'Take out rubbish'
    }]
  };

  /**
   * Add a new todo.
   * @param {string} title - title of todo to add.
   * @return {*} new state with todo added.
   */
  add = title => this.setState(state => ({
    todos: [
      ...state.todos,
      {
        id: uuidv1(),
        done: false,
        title
      }
    ]
  }));

  /**
   * Delete a todo.
   * @param {number} id - id of todo to delete.
   * @return {*} new state with todo removed.
   */
  delete = id => this.setState(state => ({
    todos: state.todos.filter(todo => todo.id !== id)
  }));

  /**
   * Toggle done state for the given todo.
   * @param {number} id - id of todo to toggle.
   * @return {*} new state with todo done state toggled.
   */
  toggleDone = id => this.setState(state => ({
    todos: state.todos.map(todo => todo.id === id ? {...todo, done: !todo.done} : todo)
  }));

  /**
   * Render.
   * @return {JSX} - template.
   */
  render() {
    return (
      <TodoContext.Provider
        value={{
          todos: this.state.todos,
          add: this.add,
          delete: this.delete,
          toggleDone: this.toggleDone
        }}>
        {this.props.children}
      </TodoContext.Provider>
    );
  }
}

TodoProvider.propTypes = {
  children: PropTypes.node.isRequired
};

export const TodoConsumer = TodoContext.Consumer;

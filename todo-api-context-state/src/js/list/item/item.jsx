import React from 'react';
import PropTypes from 'prop-types';

/**
 * Item component.
 */
export class Item extends React.Component {
  /**
   * Toggle this todo's done state.
   */
  toggleDone = () => {
    this.props.toggle(this.props.todo.id);
  }

  /**
   * Delete this todo.
   */
  deleteTodo = () => {
    this.props.delete(this.props.todo.id);
  }

  /**
   * Render.
   * @return {JSX} - template.
   */
  render() {
    return (
      <div className="item">
        <input className="item__done" type="checkbox" checked={this.props.todo.done} onChange={this.toggleDone}/>
        <p className={`item__title ${this.props.todo.done ? 'item__title--done' : ''}`}>{this.props.todo.title}</p>
        <button onClick={this.deleteTodo} className="item__delete">X</button>
      </div>
    );
  }
}

Item.propTypes = {
  todo: PropTypes.object.isRequired,
  delete: PropTypes.func.isRequired,
  toggle: PropTypes.func.isRequired
};

# React Training Course

---

## Suggested Approach

For the exercises, we suggest that you approach each one in a separate file / separate component.

We also suggest that you install the react dev tools for your browser, these should be available through your browsers extensions store ( ie Firefox and Chrome ).

---

## Session 1 - ES6 Refresher

### ES6

#### Classes

```
class MyClass {
  constructor() {
  }
}
```

#### Modules

Export

```
export const value = false;
export class MyClass{}
export default class MyClass()
```

Import

```
import {value} from '...';
import {MyClass} from '...';
import * from '...';
import MyClass from '...';
import {MyClass as AnotherClass} from '...';
```

#### Arrows

Short syntax for functions

```function test(a, b) {}```

can be rewritten as

```test(a,b) => {}```

Shorter return syntax

```test(a,b) => a + b```

#### Map

Tranforms an array, a new array is built to hold the transformed values, the original array is unaffected.  Map is `immutable`.

```
const array = [0, 1, 2];
const squares = array.map(value => value * value);
```

### JSDoc

You should also add JSDoc blocks above your classes and functions.

```
/**
  * My First Component
  */
class MyFirstComponent {
```

and

```
/**
  * My first function
  * @param {string} name - the name to use in the greeting message.
  * @return {string} returns the greeting message customised for the provided name
  */
  function myFirstFunction(name) {
```

**Exercise - write some simple ES6 standalone code ?**

---

## Session 2 - Introduction to Soak ReactJS Projects

### Start a ReactJS Project

From a scratch folder on your machine, run the following command to create a new folder with a scaffoled project inside, obviously replace <projectname> with an appropriate name for your new app.

```yo soak:react <projectname>```

This will create a new project in the folder `<projectname>` and install any required NPM modules

### Anatomy of a Soak ReactJS Project

```
assets/
	index.html - empty HTML template, has a dummy element to render into - <div id="contents"></div>, and a script tag for the bundled js ( output of webpack )

node_modules/
	installed dependencies

src/
	css/
		styles.scss - empty scss file, any external scss must be imported here ( due to the way webpack works )

	js/
		app.jsx - place holder ReactJS component that renders into the dummy `contents` div in `index.html`

test/
	test.js - example mocha test

.babelrc - additional transforms for ES6+ features ( rest, spread etc. )
.editorconfig - editor config for files ( eg. .jsx )
.eslintrc - eslint config for ReactJS source files
.gitignore - things not to commit to git, this includes both the doc and dist folder output of the build process
.sass-lint.yml - sass linting config
jsdoc.conf - jsdoc generation config
lint-errors.txt - sass linting output, should be empty ;)
package-lock.json - used by NPM to manage child dependencies
package.json - main project depeandancies plus build scripts
README.md - basic outline of available npm scripts, intended to be completed once project is set up
webpack.config.js - the webpack build config
```

### What ReactJS is and what ReactJS isn't

ReactJS is a library for rendering components, it isn't a full featured framework like Angular, it doesn't include support for HTTP requests, it doesn't even include a router.  Routers are available but these are 3rd party.  There are a lot of NPM modules available for ReactJS.

### Available NPM commands

By default a number of NPM commands are already available in the package.json.
```
npm run serve - this serves your project for viewing in the browser ( runs from memory )
npm run lint - this will run ESLint over your code
npm run build - this will generate the dist folder, with production assets
npm run test - this will run any js unit tests defined in the test folder ( or child folders )
npm run watch - this is similar to serve, but runs with files on disk
npm run lint-style - this will lint your scss
```

**Exercise - create an empty ReacJS project, and run it**

---

## Session 3 - React/ES6 Usage

### Components

ReactJS components derive from a base component class provided by ReactJS, when overriding the default constructor special care must be taken to pass the param props to the parent constructor

```
class MyClass extends React.Component {
  constructor(props) {
    super(props);
  }
}
```

Here we've invoked the parent constructor with `super` and passed along the param `props` that was given to us.

As an aside simple function based components can be written in ReactJS in this instance they are nothing more than a render function that takes props as a param.

```
function myComponent(props) {
  return (<h1>{props.title}</h1>);
}
```

Generally, this isn't used for components, as they have state or logic that's beyond this simple construct.

#### Arrows

In ReactJS we use this to define member functions in classes, using this notation ensures that `this` is available within the function to refer to the instance.

```myFunc = a => a * a;```

### JSX

HTML style mark up in JS

```
return (
  <h1>Title</h1>
);
```

Some properties are renamed to get around conflicts with JS keywords, for example `class` is `className`

```<h1 className="title>Title</h1>```

The JSX mark up is not actually used, but recompiled through babel to code that actually creates the mark up.

```
const element = {
  type: 'h1',
  props: {
    className: 'title',
    children: 'Title'
  }
};
```

Because JSX is in code it can mix and match between HTML and JS, for example, using variables is possible with `{}` syntax

```
const title='Title';

<h1>{title}</h1>
```

Conditional logic can be used to include a piece of template or not

```
{unreadMessages.length > 0 &&
  <h2>
    You have {unreadMessages.length} unread messages.
  </h2>
}
```

or even

```
{isLoggedIn ? (
  <LogoutButton onClick={this.handleLogoutClick} />
) : (
  <LoginButton onClick={this.handleLoginClick} />
)}
```

and map can be used to output a template for each item in a list

```{numbers.map((number) => <ListItem key={number.toString()} value={number} />)}```

please pay attention to `key` here, ReactJS needs to differentiate between each of the items in the DOM, it does this with `key` when an arary of JSX is returned

### Single Parent

Much of ReactJS expects a single parent, for example a component render must return a single parent, however sometimes we may not want to insert a div etc., ReactJS provides a dummy DOM element called `React.Fragment` to get around this.  This element is compiled out in JSX.

Note - every class based component must include a render function.

```
render() {
  return (
    <React.Fragement>
      <ComponentA/>
      <ComponentB/>
    </React.Fragement>
  );
}
```

### Import / Export

Any components ( beyond app ) will need to be exported so that they can be imported when used in another compomnents render.

```
export class MyComponent extends React.Component {
```

```
import {MyComponent} from 'myComponent.jsx';
...
  render() {
    return (
      <MyComponent/>
    );
  }
```

**Exercise - Write a simple child component**

---

## Session 4 - React Compomnent Lifecycle

### Lifecycle

The main lifecycle call back of a component is render(), every ReactJS component must have a render() function, this should return the JSX required to render the component.

```
render() {
  return (<p>My Component</p>);
}
```

Render is called as soon as the component is added to the DOM, and in response to any changes to it.

There are lifecycle callbacks for when the component is added to the DOM ( both before and after ) and before the component is removed from the DOM.

>`componentWillMount`
>`componentDidMount`
>`componentWillUnmount`

**Exercise - Add lifecycle functions to your component and log when they fire**

---

## Session 5 - Component Props

### Props

Props is ReactJS's name for custom attribute that can be passed to a component.

```<MyComponent value=5 title="cake"/>```

Here both `value` and `title` are props of the component `MyComponent`.

Props can also be variables, so

```
const title = 'cake';
<MyComponent value=5 title={title}/>
```

Note - prop values are generally defined in the component that embeds the child that the prop is passed to, be this app or some other component.  This isn't a hard rule, props can be defined at any level in the hierarchy and passed around.

They can be accessed within the instance of the component by `this.props`, so `this.props.title` will return `cake`.

Props don't have to be just values but functions can also be passed.  This is important when we consider that ReactJS implements what is known as uni-directional data flow, this means that values can be passed from parent to child component, but not back, ie we can't change `this.props.title` to a value other than `cake` and expect the parent to be aware.  In fact props are immutable and can't be changed at all :/

Events, these passed down functions, are the only way to communicate up the DOM.

Props can be validated in ReactJS, ie we can set the type we expect, and enforce that it is set by the parent, or specify a default value when it's optional ( this default value is assigned to the prop when it is not specified ).

```
MyComponent.propTypes = {
  value: PropTypes.number.isRequired,
  title: PropTypes.string
};

MyComponent.defaultProps = {
  title: ''
};
```

**Exercise - Add props to your component**

---

## Session 6 - State

### State

Props are immutable, they are set by the parent and can't be changed within the component itself.

State is available within components to allow for mutable values.  Why should i use state and not just properties on my class ?  Props and state are special in that ReactJS watches both for changes, and on change re-calls the render function to redraw the component in response to these changes.

State is initialised either in the class itself

```
class MyComponent() {
    state = {
        title: 'Mutable Title'
    };
}
```

or in the constructor

```
class MyComponent() {
  constructor(props) {
    super(props);

    this.state = {
      title: 'Title from constructor'
    };
  }
}
```

State can be read with `this.state.title`

```<h1>{this.state.title}</h1>```

However can not be assigned, a `setState` function is used to update state.

```
this.setState({
  title: 'new title'
});
```

The object passed to setState is merged with the current state, overwriting any values passed, but leaving all other values intact.

However, there is another format to the function `setState`, one that takes a function instead, we use this syntax when we need to read either the existing state or props.

```
this.setState((state, props) => {
  count: state.count + 1
});
```

Note - we MUST use this second version when we need to read state or props.

The reason for this is that state isn't immediately updated, so if we just read count from `this.state.count` we can't guarantee that it has the most up-to-date value, therefore the count could potentially not increment correctly.

### Lifting State

As we've seen state exists within a component, and as such is only available within that component, it can be passed to child components as props, and send to parents via events.  But in neither instance can it be changed.  In ReactJS we 'lift state' to shared parents if something is required by siblings, for example if we had a page with a list display component and an add new item component on it, neither of those components can hold the list itself as it wouldn't be available to the other, instead we lift the state to the first shared parent of the two components.  The list display component can then be passed the current list as a prop for rendering, and the add component can emit an event to add a new item to the list.

**Exercise - use state in your component**

---

## Session 7 - Wrapping other components

### Child Components in a wrapping parent

It's possible to build components that wrap dynamic child components

```
<MyParentComponent>
  <MyChildComponent/>
</MyParentComponent>
```

In order to render `MyChildComponent` in `MyParentComponent` we must use `props.children`, the render of `MyParentComponent` might look something like this

```
return (
  <div>
    {props.children}
  </div>
);
```

Children is provided by React itself and shouldn't need to be defined, however if you do need to define it, you can do that like this -
```
MyComponent.propTypes = {
  children: PropTypes.node.isRequired,
};
```

**Exercise - Wrap your component in another component that renders it**

---

## Session 8 - Router

### Router

We can wrap route based components in a router to set up different content based on the current URL.

```
import {BrowserRouter, Route, Switch} from 'react-router-dom';

<BrowserRouter>
  <Switch>
    <Route exact path="/" component={List}/>
    <Route exact path="/about" component={About}/>
  </Switch>
</BrowserRouter>
```

Note - as a rule the BrowserRouter element would appear in app.jsx, with any content provided via a Route, only static content ( ie content that doesn't change ) such as a site header and footer would be outside the router.

We use the `switch` component from the router library to ensure that only the first route match is used, without this it would render each route that passes.

The `exact` property is used to ensure the whole URL matches the path we've specified, for example all routes begin `/`, so without the `exact` property on the `/` path, we would only ever see the home page component.

`component` specifies which component we want to render for the route.

When using a route we shouldn't use HTML anchor tags to navigate, these can cause the app to reload - potentially losing any existing state, instead we should use a route link component.

```
import {Link} from 'react-router-dom';

<Link to="/">
```

Where `to` will match the path in our routes.

Note - links appear in the child components that are rendered by a Route, ie List or About in the example above.  And they would link to alternative content, ie List would have a link to About and visa versa, ie <Link to="/about>.

**Exercise - implement a router with 2 pages, add links to each page from the other**
Note - We suggest here that you have 2 components, that navigate between each other, ie Component A links to Component B, and visa versa.

Bonus points if you can link all your previous sessions together in a single app, with navigation between each one.

---

## Session 9 - Events

### Events

React has properties for all HTML events, this can be used to call functions in response to HTML events without the need to explicity add event listners, for example to get a click

```
handleClick = () => console.log('You clicked!');

render() {
  return (<h1 onClick={this.handleClick}>Click Me!</h1>);
}
```

**Exercise - add a simple event to log in a component on click**

---

## Session 10 - Forms

### Forms

Form controls, HTML5 inputs, can be a little clunky in ReactJS, we need to give it a value to display but update the value ourselves in response to changes, it looks a little like this

```
this.state = {value: ''};

handleChange = event => this.setState({value: event.target.value});

<input type="text" value={this.state.value} onChange={this.handleChange} />
```

**Exercise - write a simple form that logs the field values on submit**

---

## Session 11 - App state with Context API

### Context API

Context API is a simple mechanism in ReactJS for sharing state across an app, it's a single component that doesn't render anything more than it's children.  Although we only write one component, we actually end up with 2 - the provider and the consumer.

So, if we start with a new class / compomnent which will be our provider component.

At the top of this provider component, we start by declaring a context

```const TodoContext = React.createContext();```

Then create a component that renders the provider around it's children.

```
render() {
  return (
    <TodoContext.Provider
      value={{
        todos: this.state.todos,
        add: this.add,
        delete: this.delete,
        toggleDone: this.toggleDone
      }}>
      {this.props.children}
    </TodoContext.Provider>
  );
}
```

`value` determines what the child components can access, this can be data or events.

Note - anything exposed by the value object must be defined within the Provider component, ie todos in the example above will require a state block to be defined with `todos`, this is omitted from the snippet above.

Finally we need to export the consumer as well as the component itself.

```export const TodoConsumer = TodoContext.Consumer;```

To use this we need to insert two elements in to the DOM, the provider and then nested inside the consumer.

```
<TodoProvider>
  <TodoConsumer>
    {list => (
      <Add
        add={list.add}
      ></Add>
    )}
  </TodoConsumer>
  <TodoConsumer>
    {list => list.todos.map(todo => (
      <Item
        todo={todo}
        key={todo.id}
        delete={list.delete}
        toggle={list.toggleDone}
      ></Item>
    ))}
  </TodoConsumer>
</TodoProvider>
```

These can appear at any point in the DOM, although the Consumer must be below the Provider, there should only be a single provider, but there can be multiple consumers, whenever the provider's services are required.

* This is a bad example, obviously we could wrap both components in the same consumer, but it demonstrates the ability to have multiple consumers.

**Exercise - write a simple TODO app with Context API for app state**

---

## Session 12 - App State with Redux

### Redux

Redux is a much bigger concept, but ultimately similar to context API, it allows us to have a single global state ( known as a `store` ), that is available to any component within the DOM and exposed through props.

In redux speak `actions` are dispatch to `resolvers` that mutate the store.  The mutated store is then passed back to any component connected to it.

Let's state with setting up a store.

```
import {applyMiddleware, createStore} from 'redux';
import thunk from 'redux-thunk';

import {rootReducer} from './root-reducer.jsx';

const initialState = {
  todos: {
    list: [{
      id: 0,
      done: false,
      title: 'Get shopping'
    }, {
      id: 1,
      done: true,
      title: 'Have coffee'
    }, {
      id: 2,
      done: false,
      title: 'Take out rubbish'
    }]
  }
};

export const store = createStore(
  rootReducer,
 initialState,
  applyMiddleware(thunk)
);
```

We use `thunk` to allow for asynchronous actions.  Here we are setting up our reducers, our initial state and enabling `thunk`.

Next let's look at our reducers, firstly our root reducer

```
import {combineReducers} from 'redux';

import {todoReducer} from './reducers/todo-reducer.jsx';

export const rootReducer = combineReducers({
  todos: todoReducer
});
```

Essentially we want a reducer for each state block ( root property ) in our store, we only have `todos` so we just set `todoReducer` as that blocks reducer.

And that reducer looks something like this

```
import {ADD_TODO} from '../actions/add.jsx';
import {DELETE_TODO} from '../actions/delete.jsx';
import {TOGGLE_TODO} from '../actions/toggle-done.jsx';

const initialState = {
  list: []
};

export const todoReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_TODO:
      return {
        ...state,
        list: [
          ...state.list,
          {
            ...
          }
        ]
      };
    case DELETE_TODO:
      return {
        ...state,
        list: state.list.filter(todo => todo.id !== action.id)
      };
    case TOGGLE_TODO:
      return {
        ...state,
        list: state.list.map(todo => {
          ...
        })
      };
    default: return state;
  }
};
```

Basically the reducer is a big switch statement that responds to defined actions, based on the action the store is mutated, ie add - adds a new TODO to the store, delete removes one etc.

A new state is always returned, regardless of the action.  Including a default to just the state unaffected.  One thing to bare in mind here is that the state is immutable, you must create a new state if you want to change anything.

Also, note this is a reducer for the todos state block, therefore it's initial state is only the children of that root property in the store, not the root property itself.

Our actions are fairly straight forward, we simply have a `type` which tells the reducer how we want to mutate state, plus any payload.

Here's a delete action for example

```
export const DELETE_TODO = 'DELETE_TODO';

export const deleteTodo = id => ({
  type: DELETE_TODO,
  id: id
});
```

Now we have our 3 aspects of Redux - store / reducer / action, we just need to allow our components to use it.

In our app we wrapper everything with a redux provider passing it our store.

```
import {Provider} from 'react-redux';

import {store} from './state/store.jsx';

<Provider store={store}>
  ...
</Provider>
```

Then individual components must be configured to use it, basically it's exposed through `props`.  At the end of our component we need to connect the store, we can connect to the whole store or any property, regardless of if it's root or a child of a child of a child etc.  And the actions we like to use.

```
export const List = connect(state => ({
  todos: state.todos.list
}), {
  addTodo,
  deleteTodo,
  toggleDone
})(ListComponent);
```

Here we've connected the List component to todo's list in state, and allowed it to use the add / delete / toggle actions.

Note we export the connected component not the class as we do with a normal component.

The List component can then use any of these via props as if they were passed by it parent, ie the todo list can be accessed simply by `this.props.todos`.

**Exercise - update your Context API app to use Redux**

---

## External Resources

### ReactJS Documentation
[React Docs](https://reactjs.org/docs/getting-started.html)

### Traversy ReactJS Crash Course
[Youtube](https://www.youtube.com/watch?v=sBws8MSXN7A)

### Traversy Context API
[Youtube](https://www.youtube.com/watch?v=NDEt0KdDbhk&list=PLillGF-RfqbaxgxkKgKk1XlJAVCX31xRI)

### Traversy Redux Crash Course
[Youtube](https://www.youtube.com/watch?v=93p3LxR9xfM)






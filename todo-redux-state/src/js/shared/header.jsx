import React from 'react';

import {Link} from 'react-router-dom';

/**
 * Header component.
 */
export class Header extends React.Component {
  /**
   * Render.
   * @return {JSX} - template.
   */
  render() {
    return (
      <div className="title">
        <h1 className="title__title">Todo List</h1>
        <p className="title__nav"><Link className="link" to="/">Home</Link> | <Link className="link" to="/about">About</Link></p>
      </div>
    );
  }
}

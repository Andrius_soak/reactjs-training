import {applyMiddleware, createStore} from 'redux';
import thunk from 'redux-thunk';

import {rootReducer} from './root-reducer.jsx';

// Initial state, used to initialise the store
const initialState = {
  todos: {
    list: [{
      id: 0,
      done: false,
      title: 'Get shopping'
    }, {
      id: 1,
      done: true,
      title: 'Have coffee'
    }, {
      id: 2,
      done: false,
      title: 'Take out rubbish'
    }]
  }
};

// Build the initial store, defining Reducers, initail state and any middleware we want to use
export const store = createStore(
  rootReducer,
  initialState,
  applyMiddleware(thunk)
);

// Reducer action type
export const TOGGLE_TODO = 'TOGGLE_TODO';

/**
 * Create a toggle action for the reducer to handle.
 * @param {string} id - id of todo to toggle.
 * @return {*} action for reducer.
 */
export const toggleDone = id => ({
  type: TOGGLE_TODO,
  id: id
});

// Reducer action type
export const DELETE_TODO = 'DELETE_TODO';

/**
 * Create a delete action for the reducer to handle.
 * @param {string} id - id of todo to delete.
 * @return {*} action for reducer.
 */
export const deleteTodo = id => ({
  type: DELETE_TODO,
  id: id
});

import {ADD_TODO} from '../actions/add.jsx';
import {DELETE_TODO} from '../actions/delete.jsx';
import {TOGGLE_TODO} from '../actions/toggle-done.jsx';

// Initial state, used when no previous state exists
const initialState = {
  list: []
};

/**
 * Update state according to an action.
 * @param {*} state - current state.
 * @param {*} action - action to modify state, including payload.
 * @return {*} new state post action.
 */
export const todoReducer = (state = initialState, action) => {
  // handle actions
  switch (action.type) {
    // add
    case ADD_TODO:
      return {
        ...state,
        list: [
          ...state.list,
          {
            id: action.id,
            done: false,
            title: action.title
          }
        ]
      };
    // delete
    case DELETE_TODO:
      return {
        ...state,
        list: state.list.filter(todo => todo.id !== action.id)
      };
    // toggle done state
    case TOGGLE_TODO:
      return {
        ...state,
        list: state.list.map(todo => {
          if (todo.id === action.id) {
            return {
              ...todo,
              done: !todo.done
            };
          } else {
            return todo;
          }
        })
      };
    // unhandled action, no change to state
    default: return state;
  }
};

import {combineReducers} from 'redux';

import {todoReducer} from './reducers/todo-reducer.jsx';

// mapping between reducers and state blocks
export const rootReducer = combineReducers({
  todos: todoReducer
});

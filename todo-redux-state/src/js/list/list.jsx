import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';

import {Add} from './add/add.jsx';
import {addTodo} from '../state/actions/add.jsx';
import {deleteTodo} from '../state/actions/delete.jsx';
import {Item} from './item/item.jsx';
import {toggleDone} from '../state/actions/toggle-done.jsx';

/**
 * List component.
 */
class ListComponent extends React.Component {
  /**
   * Render.
   * @return {JSX} - template.
   */
  render() {
    return (
      <React.Fragment>
        <Add
          add={this.props.addTodo}
        ></Add>
        {this.props.todos.map(todo => (
          <Item
            todo={todo}
            key={todo.id}
            delete={this.props.deleteTodo}
            toggle={this.props.toggleDone}
          ></Item>
        ))}
      </React.Fragment>
    );
  }
}

ListComponent.propTypes = {
  // validate store connnections ( these appear as props )
  addTodo: PropTypes.func.isRequired,
  deleteTodo: PropTypes.func.isRequired,
  toggleDone: PropTypes.func.isRequired,
  todos: PropTypes.array.isRequired
};

// Define store connections ( without this we won't be passed store state, all be re-rendered when it changes )
export const List = connect(state => ({
  // connect store state blocks
  todos: state.todos.list
}), {
  // connect actions
  addTodo,
  deleteTodo,
  toggleDone
})(ListComponent);

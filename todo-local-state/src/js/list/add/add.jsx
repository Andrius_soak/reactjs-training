import React from 'react';
import PropTypes from 'prop-types';

/**
 * Add component.
 */
export class Add extends React.Component {
  state = {
    title: ''
  };

  /**
   * Change title.
   * @param {Event} ev - change event.
   */
  changeTitle = ev => {
    this.setState({title: ev.target.value});
  }

  /**
   * Add new todo.
   */
  add = () => {
    this.props.add(this.state.title);

    // clear previous input
    this.setState({title: ''});
  }

  /**
   * Render.
   * @return {JSX} - template.
   */
  render() {
    return (
      <div className="add">
        <input className="add__input" type="text" value={this.state.title} onChange={this.changeTitle} placeholder="Add Todo ..."/>
        <button onClick={this.add} className="add__button">Add</button>
      </div>
    );
  }
}

Add.propTypes = {
  add: PropTypes.func.isRequired
};
